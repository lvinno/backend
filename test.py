import numpy as np
import os
import matplotlib.pyplot as plt
import keras
from keras.preprocessing import image
from keras.applications.inception_v3 import preprocess_input
from keras.backend import clear_session
import tensorflow as tf

global graph
graph = tf.get_default_graph()

def show_prediction(model):
    route = 'predict'
    path = os.getcwd()+'/'+route+'/'
    files= os.listdir(path)
    model_input_details = model.get_input_details()
    model_output_details = model.get_output_details()
    #keras.backend.clear_session()

    #model = load_model('./save.h5')
    classes = ['Tabby','Persian','Siamese','Egyptian']
    cat_identifier = {'category':'cat', 'certainity':'rate'}

    

    for file in files:
        img = image.load_img(path+file, target_size=(299,299,3))
        img_array = image.img_to_array(img)
        img_array_expanded_dims = np.expand_dims(img_array, axis=0)
        processed_img = preprocess_input(img_array_expanded_dims)
        
        with graph.as_default():
            model.set_tensor(model_input_details[0]['index'],processed_img)
            model.invoke()
            prediction = model.get_tensor(model_output_details[0]['index'])

        prediction_index = prediction.argmax()
        prediction = np.max(prediction)


        cat_identifier['category'] = str(classes[int(prediction_index)])
        cat_identifier['certainity'] = str(prediction*100)
        break
    
    return cat_identifier

#show_prediction()


