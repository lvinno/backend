from flask import Flask, render_template, Response, jsonify, request
import json
from werkzeug.utils import secure_filename
from flask_cors import CORS
import test
import os
import os.path as path
from tensorflow.contrib.lite.python import interpreter as interpreter_wrapper
from keras.models import load_model
import tensorflow as tf


app = Flask(__name__)
CORS(app, supports_credentials=True)

model = interpreter_wrapper.Interpreter(model_path='./tflitefile.tflite')
model.allocate_tensors()


@app.route('/catAnalysis', methods = ['POST'])
def root():
 
    cat = request.files['image']
  #  app.config['dir'] = "./predict"
  #  cat_name = 'cat.jpg'
  #  cat.save(os.path.join(app.config['dir'], cat_name))
    basepath = path.abspath(path.dirname(__file__))
    filename = 'cat.jpg'
    upload_path=path.join(basepath,'predict',filename)
    cat.save(upload_path)
    print ('upload successfully', cat)
    print (cat.filename)

    # return jsonify({'category':'2', 'certainity':'34'})
    prediction = test.show_prediction(model)

    # return Response(json.dumps(tabel), mimetype = 'application/json')
    return jsonify(prediction)
        
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8090,debug=True)